define(['knockout', 'application'], function (ko, application) {
    return function () {
        this.pack = ko.observable({
            name: ko.observable(''),
            cards: ko.observableArray([])
        });

        this.delete = new application.Event();
        this.select = new application.Event();

        this.activated.on(function (e, p) {
            this.pack(p);
        }.bind(this));

        this.loaded.on(function (e) {
            this.view().on('click', '.delete-pack', function (e) {
                this.delete.trigger(this.pack());
                e.stopPropagation();
            }.bind(this));

            this.view().on('click', '.pack-desc', function (e) {
                this.select.trigger(this.pack());
            }.bind(this));
        }.bind(this));
    };
});
