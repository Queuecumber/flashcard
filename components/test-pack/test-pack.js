define(['knockout', 'application'], function (ko, application) {
    return function () {
        this.pack = ko.observable({
            name: '',
            cards: ko.observableArray([])
        });

        this.newIndex = function () {
            return Math.floor(Math.random() * this.pack().cards().length);
        }.bind(this);

        this.index = ko.observable(0);

        this.current = ko.computed(function () {
            return this.pack().cards()[this.index()];
        }.bind(this));

        this.activated.on(function (e, k) {
            var c = application.model().packs().find(function (c) { return c.key() === k; });
            this.pack(c);

            this.pack().cards.subscribe(function (c) {
                this.index(this.newIndex());
            }, this, 'arrayChange');

            this.index(this.newIndex());

            application.name(c.name());
        }.bind(this));

        this.loaded.on(function (e) {
            this.view().on('click', '.test-done', function () {
                application.router.route(application.cards(), this.pack());
            }.bind(this));

            this.card().next.on(function () {
                this.index(this.newIndex());
            }.bind(this));

            this.current.subscribe(function () {
                this.card().activate(this.current());
            }.bind(this));
        }.bind(this));

        application.router.add('test', '{key}', this);
    };
});
