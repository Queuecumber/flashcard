define(['knockout', 'application', 'card'], function (ko, application, Card) {
    return function () {
        this.card = ko.observable({
            front: ko.observable(),
            back: ko.observable()
        });

        this.editmode = false;

        this.built = new application.Event();

        this.activated.on(function (e, card) {
            if(card) {
                this.card(card);
                this.editmode = true;
                this.view().find('label').addClass('active');
            }

            this.view().find("#build-card").openModal({
                complete : this.finish.bind(this)
            });
        }.bind(this));

        this.finished.on(function (e) {
            this.card({
                front: ko.observable(''),
                back: ko.observable('')
            });
            this.editmode = false;
            this.view().find('label').removeClass('active');
        }.bind(this));

        this.loaded.on(function (e) {
            this.view().on('click', '.card-go', function (e) {
                if(!this.editmode) {
                    var ncard = new Card({
                        key: null,
                        value: {
                            front: this.card().front(),
                            back: this.card().back()
                        }
                    });

                    this.built.trigger(ncard);
                }

                this.view().find("#build-card").closeModal();
            }.bind(this));
        }.bind(this));
    };
});
