define(['knockout', 'application', 'card'], function (ko, application, Card) {
    return function () {
        this.card = ko.observable({
            front: ko.observable(''),
            back: ko.observable('')
        });

        this.next = new application.Event();

        this.activated.on(function (e, card) {
            this.card(card);
        }.bind(this));

        this.loaded.on(function (e) {
            this.view().on('click', '.next-card', function () {
                this.next.trigger();
            }.bind(this));
        }.bind(this));
    };
});
