define(['knockout', 'application', 'pack'], function (ko, application, Pack) {
    return function () {
        this.pack = ko.observable({
            name: ko.observable(''),
            cards: ko.observableArray([])
        });

        this.editmode = false;

        this.built = new application.Event();

        this.activated.on(function (e, pack) {
            if(pack) {
                this.pack(pack);
                this.editmode = true;
                this.view().find('label').addClass('active');
            }

            this.view().find("#build-pack").openModal({
                complete : this.finish.bind(this)
            });
        }.bind(this));

        this.finished.on(function (e) {
            this.pack({
                name: ko.observable(''),
                cards: ko.observableArray([])
            });
            this.editmode = false;
            this.view().find('label').removeClass('active');
        }.bind(this));

        this.loaded.on(function (e) {
            this.view().on('click', '.pack-go', function (e) {
                if(!this.editmode) {
                    var npack = new Pack({
                        key: null,
                        value: {
                            name: this.pack().name(),
                            cards: this.pack().cards()
                        }
                    });

                    this.built.trigger(npack);
                }

                this.view().find("#build-pack").closeModal();
            }.bind(this));
        }.bind(this));
    };
});
