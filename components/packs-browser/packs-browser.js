define(['knockout', 'application', 'pack'], function (ko, application, pack) {
    return function () {
        this.packs = ko.computed(function () {
            return application.model().packs();
        });

        this.activated.on(function (e) {
            application.name("Packs");
        });

        this.loaded.on(function (e) {
            this.builder().built.on(function (e, pack) {
                application.model().packs.push(pack);
            });

            this.view().on('click', '.add-pack', function (e) {
                this.builder().activate();
            }.bind(this));

            this.previews().select.on(function (e, p) {
                this.finish();
                application.router.route(application.cards(), p);
            }.bind(this));

            this.previews().delete.on(function (e, p) {
                application.model().packs.remove(p);
            });
        }.bind(this));

        application.router.add('/', '', this);
    };
});
