define(['knockout', 'application', 'card'], function (ko, application, Card) {
    return function () {
        this.pack = ko.observable({
            name: '',
            cards: ko.observableArray([])
        });

        this.activated.on(function (e, k) {
            var c = application.model().packs().find(function (c) { return c.key() === k; });
            this.pack(c);

            application.name(c.name());
        }.bind(this));

        this.loaded.on(function (e) {
            this.builder().built.on(function (e, card) {
                this.pack().cards.push(card);
            }.bind(this));

            this.previews().edit.on(function (e, card) {
                this.builder().activate(card);
            }.bind(this));

            this.previews().delete.on(function (e, card) {
                this.pack().cards.remove(card);
            }.bind(this));

            this.previews().fullscreen.on(function (e, card) {
                this.view().find('#card-fullscreen').openModal({
                    complete : this.fullscreen().finish.bind(this.fullscreen)
                });
                this.fullscreen().activate(card);
            }.bind(this));

            this.fullscreen().next.on(function (e) {
                this.view().find('#card-fullscreen').closeModal();
            }.bind(this));

            this.view().on('click', '.add-card', function (e) {
                this.builder().activate();
            }.bind(this));

            this.view().on('click','.test-pack', function (e) {
                application.router.route(application.test(), this.pack());
            }.bind(this));
        }.bind(this));

        application.router.add('cards', '{key}', this);
    };
});
