define(['knockout', 'application', 'card'], function (ko, application, Card) {
    return function () {
        this.card = ko.observable({
            front: ko.observable(''),
            back: ko.observable('')
        });

        this.delete = new application.Event();
        this.edit = new application.Event();
        this.fullscreen = new application.Event();

        this.activated.on(function (e, card) {
            this.card(card);
        }.bind(this));

        this.loaded.on(function (e) {
            this.view().on('click', '.edit-card', function (e) {
                this.edit.trigger(this.card());
            }.bind(this));

            this.view().on('click', '.delete-card', function (e) {
                this.delete.trigger(this.card());
            }.bind(this));

            this.view().on('click', '.fullscreen-card', function (e) {
                this.fullscreen.trigger(this.card());
            }.bind(this));
        }.bind(this));
    };
});
