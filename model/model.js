define(['application', 'knockout', 'database', 'pack'], function (application, ko, Database, Pack) {
    var model = function () {

        this.packs = ko.observableArray([]);

        this.upgrade = new application.Event();
        this.ready = new application.Event();

        this.load = function () {
            this.db = new Database('flashcard', 3);
            this.db.ready.on(function (e) {
                this.db.get('packs', function (packData) {
                    packData
                        .map(function (c) { return new Pack(c); })
                        .forEach(function (c) { this.packs.push(c); }, this);

                    this.packs.subscribe(storeupdate, this, 'arrayChange');
                    this.ready.trigger();
                }.bind(this));
            }.bind(this));
        };

        var handleAdd = function (additions) {
            additions.forEach(function (c) {
                this.db.put('packs', { key: null, value: c.serialize() }, function(k) {
                    c.key(k);
                });
            }, this);
        }.bind(this);

        var handleRemove = function (removals) {
            removals.forEach(function (c) {
                this.db.delete('packs', c.key());
            }, this);
        }.bind(this);

        var storeupdate = function (changes) {
            var adds = changes.filter(function (c) { return c.status === 'added'; }).map(function (c) { return c.value; });
            var rems = changes.filter(function (c) { return c.status === 'deleted'; }).map(function (c) { return c.value; });

            handleAdd(adds);
            handleRemove(rems);
        }.bind(this);
    };

    return model;
});
