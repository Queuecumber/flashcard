define(['knockout', 'application'], function (ko, application) {
    var card = function (config) {
        var db = application.model().db;

        this.front = ko.observable(config.value.front);
        this.back = ko.observable(config.value.back);
        this.key = ko.observable(config.key);

        var saveSelf = function () {
            db.put('cards', { key: this.key(), value: this.serialize() });
        }.bind(this);

        this.front.subscribe(saveSelf);
        this.back.subscribe(saveSelf);

        this.serialize = function () {
            var jsobj = ko.toJS(this);

            delete jsobj.key;
            delete jsobj.serialize;

            return jsobj;

        }.bind(this);
    };

    return card;
});
