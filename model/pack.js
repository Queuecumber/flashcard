define(['knockout', 'application', 'card'], function (ko, application, Card) {
    var pack = function (config) {
        this.cards = ko.observableArray([]);
        this.key = ko.observable(config.key);
        this.name = ko.observable(config.value.name);

        db = application.model().db;
        config.value.cards.forEach(function (ckey) {
            db.find('cards', ckey, function (cdesc) {
                var card = new Card({
                    key: ckey,
                    value: cdesc
                });

                this.cards.push(card);

                if(this.cards().length === config.value.cards.length)
                    this.cards.subscribe(storeupdate, this, 'arrayChange');
            }.bind(this));
        }, this);

        var saveSelf = function () {
            db.put('packs', { key: this.key(), value: this.serialize() });
        }.bind(this);

        var handleAdd = function (additions) {
            additions.forEach(function (c) {
                db.put('cards', { key: null, value: c.serialize() }, function(k) {
                    c.key(k);
                    saveSelf();
                });
            }, this);
        }.bind(this);

        var handleRemove = function (removals) {
            removals.forEach(function (c) {
                db.delete('cards', c.key());
                saveSelf();
            }, this);
        }.bind(this);

        var storeupdate = function (changes) {
            var adds = changes.filter(function (c) { return c.status === 'added'; }).map(function (c) { return c.value; });
            var rems = changes.filter(function (c) { return c.status === 'deleted'; }).map(function (c) { return c.value; });

            handleAdd(adds);
            handleRemove(rems);
        }.bind(this);

        if(config.value.cards.length === 0)
            this.cards.subscribe(storeupdate, this, 'arrayChange');

        this.serialize = function () {
            var jsobj = ko.toJS(this);

            delete jsobj.key;
            delete jsobj.serialize;

            jsobj.cards = jsobj.cards.map(function (c) { return c.key; });

            return jsobj;

        }.bind(this);
    };

    return pack;
});
