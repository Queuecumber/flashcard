require.config({
    baseUrl: './',
    paths: {
        knockout: 'bower_components/knockout/dist/knockout',
        requirejs: 'bower_components/requirejs/require',
        application: 'bower_components/application/application',
        crossroads: 'bower_components/crossroads/dist/crossroads.min',
        hasher: 'bower_components/hasher/dist/js/hasher.min',
        signals: 'bower_components/js-signals/dist/signals.min',
        router: 'util/router',
        database: 'util/database',
        model: 'model/model',
        pack: 'model/pack',
        card: 'model/card'

    }
});

define('jquery', [], function() {
    return jQuery;
});

require(['application', 'model', 'router'], function (application, model, Router)
{
    var launch = function () {
        $('.modal-trigger').leanModal();
        $(".button-collapse").sideNav();

        application.loading().finish();
        application.omni().activate();
        
        application.router.launch();
    };

    var m = new model();
    m.ready.on(function (f) {
        launch();
    });

    application.loaded.on(function () {
        m.load();
    });

    application.router = new Router();
    application.model(m);
    application.compose();
});
