define(['application'], function (application) {
    return function (name, version) {
        var dbreq = indexedDB.open(name, version);
        this.db = null;

        this.upgrade = new application.Event();
        this.ready = new application.Event();

        dbreq.onupgradeneeded = function (e) {
            this.upgrade.trigger();

            db = e.target.result;

            if(!db.objectStoreNames.contains("packs")) {
                db.createObjectStore("packs", { autoIncrement: true });
            }

            if(!db.objectStoreNames.contains("cards")) {
                db.createObjectStore("cards", { autoIncrement: true });
            }
        }.bind(this);

        dbreq.onsuccess = function (e) {
            this.db = e.target.result;
            this.ready.trigger();
        }.bind(this);

        this.get = function (table, callback) {
            var result = [];
            this.db.transaction([table], "readonly")
                .objectStore(table)
                .openCursor()
                .onsuccess = function (e) {
                    var cursor = e.target.result;

                    if(cursor) {
                        result.push({
                            key: cursor.key,
                            value: cursor.value
                        });
                        cursor.continue();
                    } else {
                        if(callback)
                            callback(result);
                    }
                };
        }.bind(this);

        this.find = function (table, key, callback) {
            this.db.transaction([table], "readonly")
                .objectStore(table)
                .get(key)
                .onsuccess = function (e) {
                    if(callback)
                        callback(e.target.result);
                };
        }.bind(this);

        this.put = function (table, data, callback) {
            var os = this.db.transaction([table], "readwrite").objectStore(table);

            var req = null;
            if(data.key !== null) {
                req = os.put(data.value, data.key);
            } else {
                req = os.add(data.value);
            }

            req.onsuccess = function (e) {
                if(callback)
                    callback(e.target.result);
            };
        }.bind(this);

        this.delete = function (table, key, callback) {
            this.db.transaction([table], "readwrite")
                .objectStore(table)
                .delete(key)
                .onsuccess = function (e) {
                    if(callback)
                        callback();
                };
        }.bind(this);
    };
});
