define(['application', 'hasher', 'crossroads'], function (application, hasher, crossroads) {

    return function () {
        var parseHash = function (newHash, oldHash) {
            application.children().forEach(function (c) {
                if (c.active() && c !== application.omni()) 
                    c.finish();
            });

            crossroads.parse(newHash);
        };

        hasher.initialized.add(parseHash);
        hasher.changed.add(parseHash);

        this.launch = function () {
            hasher.init();
        };

        this.add = function (prefix, arg, component) {
            var r = prefix;

            if(arg  !== '')
                r += '/' + arg;

            crossroads.addRoute(r, function (arg) {
                if(arg !== undefined)
                    arg = Number(arg);

                component.activate(arg);
            });

            this.routes.push({
                prefix: prefix,
                arg: arg,
                component, component
            });
        };

        this.route = function (component, arg) {
            var target = this.routes.find(function (c) { return c.component === component; });
            var path = target.prefix + '/' + arg.key();
            hasher.setHash(path);
        };

        this.routes = [];
    };
});
